
def call(Map args) {
    node {

        stage("Compile") {
            sh "./mvnw clean compile"
        }

        stage("Unit Test") {
            sh "./mvnw test"
        }

        stage("Integration Test") {
            sh "./mvnw verify"
        }

        stage("Static Code Analysis: Sonar") {
            echo "Running static code analysis using Sonar"
            /*
			withSonarQubeEnv(credentialsId: Constants.SONARQUBE_CREDENTIALS_ID, installationName: Constants.SONARQUBE_INSTALLATION_NAME) {
                    sh './mvnw sonar:sonar'
            }
            */
        }

        stage("Package Artifact Jar") {
            sh "./mvnw package -DskipTests=true"
        }

        stage("Build Docker Image") {
            echo "Build Docker Image"
        }

        stage("Publish Docker Image") {
            echo "Publish Docker Image"
        }

        stage("Deploying to Dev") {
            echo "Deploying to Dev environment"
        }
    }
}
